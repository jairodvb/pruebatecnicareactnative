//This is an example code to understand WebView// 
 
import React, { Component } from 'react';
//import react in our code. 
 
import { WebView } from 'react-native-webview';
//import all the components we are going to use. 
 
export default class App extends Component {
  render() {
    return (
       <WebView
        source={{uri: 'https://pruebatecnica-a47b0.web.app/'}}
        style={{marginTop: 20}}
      />
    );
  }
}